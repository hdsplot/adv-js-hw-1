class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        // this._salary = salary * 3;
    }
    get salary() {
         return this._salary * 3;
     }
}

const programmer1 = new Programmer('Den', 26, 1000, 'js');
const programmer2 = new Programmer('Anna', 30, 1500, 'js, php');
console.log(programmer1);
console.log(programmer2);